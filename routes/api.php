<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::apiResource('/users', 'UserController');
    Route::post('/patient-login', 'LoginController@doLogin');
    Route::get('/patient-prenatal-schedule', 'PrenatalController@dateSchedule');
    Route::get('/patient-family-planning-schedule', 'FamilyPlanningController@dateSchedule');
    Route::get('/immunization-schedule', 'BabyImmunizationController@dateSchedule');
    Route::get('/admission/export', 'AdmissionController@export');
    Route::get('/baby/export', 'BabyController@export');
    Route::get('/baby-immunization/export', 'BabyImmunizationController@export');
    Route::get('/family-planning/export', 'FamilyPlanningController@export');
    Route::get('/forgot-password', 'LoginController@forgotPassword');
    Route::get('/reset-password', 'LoginController@resetPassword');
    // family planning pdf
    Route::get('/family-planning-pdf', 'FamilyPlanningController@exportPdf');
    // baby pdf
    Route::get('/baby-pdf', 'BabyController@exportPdf');
    // patient pdf
    Route::get('/patient-pdf', 'PatientController@exportPdf');
    // immunize pdf
    Route::get('/immunize-pdf', 'BabyImmunizationController@exportPdf');
    // delivery pdf
    Route::get('/admission-pdf', 'AdmissionController@exportPdf');
    Route::post('/reset-password', 'LoginController@resetPassword');
    Route::post('/change-username', 'LoginController@changeUsername');
    Route::post('/change-password', 'LoginController@changePassword');
    Route::get('/get-case-no', 'PatientController@getCaseNo');
    Route::get('/get-prenatal-history', 'PrenatalController@prenatalHistory');
    Route::get('/get-immunization-history', 'BabyImmunizationController@immunizationHistory');
    Route::apiResource('/babies', 'BabyController');
    Route::apiResource('/baby-immunize', 'BabyImmunizationController');
    // patient api
    Route::apiResource('/patients', 'PatientController');
    // prenatal visit api
    Route::apiResource('/prenatal-visit', 'PrenatalVisitController');
    // pregnancy history api
    Route::apiResource('/pregnancy-history', 'PregnancyHistoryController');
    // prenatal api
    Route::apiResource('/prenatals', 'PrenatalController');
    // lab result api
    Route::apiResource('/lab-results', 'LabResultController');
    // admission api
    Route::apiResource('/admissions', 'AdmissionController');
    // family planning api
    Route::apiResource('/family-plannings', 'FamilyPlanningController');
    // family planning api
    //baby immunize
    //physical exam
    Route::apiResource('/physical-exam', 'PhysicalExaminationController');
    Route::get('/prenatal-all', 'PrenatalController@getPrenatalAll');
    //prenatal visit
    // Route::apiResource('/patients', 'PatientController');
    // Route::apiResource('/prenatal-visit', 'PrenatalVisitController');
    // Route::apiResource('/prenatals', 'PrenatalController');   
    // Route::apiResource('/pregnancy-history', 'PregnancyHistoryController');
});

Route::group(['namespace' => 'Api', 'middleware' => 'auth:api'], function () {

    
});
