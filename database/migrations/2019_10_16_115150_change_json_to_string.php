<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeJsonToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prenatals', function (Blueprint $table) {
            $table->string('past_medical_history')->nullable()->change();
            $table->string('family_history')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prenatals', function (Blueprint $table) {
            $table->json('past_medical_history')->nullable()->change();
            $table->json('family_history')->nullable()->change();
        });
    }
}
