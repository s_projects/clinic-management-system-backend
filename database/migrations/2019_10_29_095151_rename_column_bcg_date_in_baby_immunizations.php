<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnBcgDateInBabyImmunizations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('baby_immunizations', function (Blueprint $table) {
            $table->renameColumn('`bcg date`', '`bcg_date`')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('baby_immunizations', function (Blueprint $table) {
            $table->renameColumn('`bcg_date`', '`bcg date`')->nullable();
        });
    }
}
