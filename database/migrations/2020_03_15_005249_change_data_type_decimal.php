<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDataTypeDecimal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prenatal_visits', function (Blueprint $table) {
            $table->decimal('weight', 8,2)->nullable()->change();
            $table->decimal('height', 8,2)->nullable()->change();
            $table->decimal('temp', 8,2)->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prenatal_visits', function (Blueprint $table) {
            $table->integer('weight')->nullable()->change();
            $table->integer('height')->nullable()->change();
            $table->integer('temp')->nullable()->change();
        });

    }
}
