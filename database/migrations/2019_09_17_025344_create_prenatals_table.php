<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrenatalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prenatals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_id');
            $table->string('menarche')->nullable();
            $table->string('amt')->nullable();
            $table->string('lmp')->nullable();
            $table->string('age_of_gestation')->nullable();
            $table->string('parity')->nullable();
            $table->string('living')->nullable();
            $table->date('tt_date_given')->nullable();
            $table->string('place_of_delivery')->nullable();
            $table->string('complication')->nullable();
            $table->string('history_of_bleeding')->nullable();
            $table->string('subsequent_menses')->nullable();
            $table->date('pmp')->nullable();
            $table->date('edc')->nullable();
            $table->string('gravida')->nullable();
            $table->string('abortion')->nullable();
            $table->string('tt')->nullable();
            $table->string('type_of_delivery')->nullable();
            $table->string('weight')->nullable();
            $table->string('family_planning_used')->nullable();
            $table->json('past_medical_history')->nullable();
            $table->json('family_history')->nullable();
            $table->string('philhealth_member')->nullable();
            $table->string('chief_complaint')->nullable();
            $table->string('referred_by')->nullable();
            $table->string('category')->nullable();

            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prenatals');
    }
}
