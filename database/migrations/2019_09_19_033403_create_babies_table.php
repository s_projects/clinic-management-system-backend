<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBabiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('babies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('baby_firstname');
            $table->string('baby_middlename')->nullable();
            $table->string('baby_lastname');
            $table->string('sex');
            $table->time('time_of_delivery');
            $table->string('mother_name')->nullable();
            $table->string('father_name')->nullable();
            $table->text('address');
            $table->string('circumference')->nullable();
            $table->string('head_cm')->nullable();
            $table->string('chest_cm')->nullable();
            $table->string('abdomin_cm')->nullable();
            $table->string('length_cm')->nullable();
            $table->string('apgar_score')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('babies');
    }
}
