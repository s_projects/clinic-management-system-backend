<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyPrenatalToPrenatalVisits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prenatal_visits', function (Blueprint $table) {
            $table->unsignedBigInteger('prenatal_id');
            $table->foreign('prenatal_id')->references('id')->on('prenatals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prenatal_visits', function (Blueprint $table) {
            $table->dropForeign(['prenatal_id']);
            $table->dropColumn('prenatal_id');
        });
    }
}
