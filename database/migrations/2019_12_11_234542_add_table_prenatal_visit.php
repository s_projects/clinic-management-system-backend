<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTablePrenatalVisit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prenatal_visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_id');
            $table->integer('aog');
            $table->string('bp');
            $table->string('fundus');
            $table->string('presentation');
            $table->string('position');
            $table->string('ext_edema');
            $table->integer('weight');
            $table->integer('height');
            $table->integer('temp');
            $table->integer('rr');
            $table->integer('pr');
            $table->string('abnormal_symptoms');
            $table->date('next_sched');
            $table->integer('pregnancy_count');

            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prenatal_visits');
    }
}
