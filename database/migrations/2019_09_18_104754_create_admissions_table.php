<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_id');
            $table->string('puft')->nullable();
            $table->string('parity')->nullable();
            $table->string('gravida')->nullable();
            $table->string('on_labor_with')->nullable();
            $table->string('final_gravida')->nullable();
            $table->string('final_puft')->nullable();
            $table->string('final_parity')->nullable();
            $table->string('abortion')->nullable();
            $table->string('apgar_score')->nullable();
            $table->string('admitted_by')->nullable();
            $table->string('disposition')->nullable();
            $table->string('living')->nullable();
            $table->string('normal_vaginal_delivery')->nullable();
            $table->string('weight')->nullable();

            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissions');
    }
}
