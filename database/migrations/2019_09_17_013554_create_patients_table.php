<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('middlename');
            $table->string('sex');
            $table->date('birthdate');
            $table->string('nationality');
            $table->string('occupation');
            $table->string('status');
            $table->string('place_of_birth');
            $table->string('age');
            $table->string('religion');
            $table->string('mobile_no');
            $table->string('spouse_firstname')->nullable();
            $table->string('spouse_lastname')->nullable();
            $table->string('spouse_middlename')->nullable();
            $table->string('spouse_birthdate')->nullable();
            $table->string('spouse_address')->nullable();
            $table->string('spouse_nationality')->nullable();
            $table->string('spouse_age')->nullable();
            $table->string('spouse_religion')->nullable();
            $table->string('spouse_occupation')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('parent_address')->nullable();
            $table->string('in_case_of_emergency')->nullable();
            $table->string('relationship_to_patient')->nullable();
            $table->string('username');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
