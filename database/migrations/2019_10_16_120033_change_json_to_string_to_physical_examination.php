<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeJsonToStringToPhysicalExamination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('physical_examinations', function (Blueprint $table) {
            $table->text('other_past_gynecological_history')->nullable()->change();
            $table->text('minor_discomfort')->nullable()->change();
            $table->text('past_medical_history')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('physical_examinations', function (Blueprint $table) {
            $table->json('other_past_gynecological_history')->nullable()->change();
            $table->json('minor_discomfort')->nullable()->change();
            $table->json('past_medical_history')->nullable()->change();
        });
    }
}
