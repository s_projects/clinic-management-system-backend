<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePregnancyHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregnancy_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_id')->nullble();
            $table->string('year')->nullble();
            $table->string('place_of_delivery')->nullble();
            $table->string('hrs_of_labor')->nullble();
            $table->string('type_of_delivery')->nullble();
            $table->text('complicaions')->nullble();
            $table->string('sex')->nullble();
            $table->string('birth_weight')->nullble();
            $table->timestamps();

            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregnancy_histories');
    }
}
