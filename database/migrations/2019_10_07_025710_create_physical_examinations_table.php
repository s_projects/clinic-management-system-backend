<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalExaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_examinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_id');
            $table->date('transaction_date')->nullable();
            $table->string('chief_complaint')->nullable();
            $table->string('present_condition')->nullable();
            $table->string('bp')->nullable();
            $table->string('temperature')->nullable();
            $table->string('weight')->nullable();
            $table->string('pr')->nullable();
            $table->string('rr')->nullable();
            $table->string('fh')->nullable();
            $table->string('fht')->nullable();
            $table->string('presentation')->nullable();
            $table->string('position')->nullable();
            $table->string('dilataion')->nullable();
            $table->string('effacement')->nullable();
            $table->string('engagement')->nullable();
            $table->string('bow')->nullable();
            $table->string('expected_date_of_delivery')->nullable();
            $table->string('gravida')->nullable();
            $table->string('para')->nullable();
            $table->string('abortion')->nullable();
            $table->string('type_of_childbirth_preparation')->nullable();
            $table->date('date_last_seen_by_doctor')->nullable();
            $table->string('allergies_sensitivities')->nullable();
            $table->string('pregnancy_problems_and_treatment')->nullable();
            $table->string('hgb')->nullable();
            $table->string('hbdag')->nullable();
            $table->string('vdrl')->nullable();
            $table->date('date_given')->nullable();
            $table->string('blood_type')->nullable();
            $table->string('rh')->nullable();
            $table->string('father_blood_type')->nullable();
            $table->string('father_rh')->nullable();
            $table->string('urinalysis')->nullable();
            $table->string('albumin')->nullable();
            $table->string('glucose')->nullable();
            $table->string('other')->nullable();
            $table->string('age_at_first_period')->nullable();
            $table->string('if_regular_period_start_every')->nullable();
            $table->string('if_irregular_period_start_every')->nullable();
            $table->string('duration_of_bleeding')->nullable();
            $table->string('bleeding_occur_between_period')->nullable();
            $table->string('bleeding_occur_after_intercourse')->nullable();
            $table->date('first_day_of_last_period')->nullable();
            $table->string('pain_associate_with_periods')->nullable();
            $table->string('if_pain_associate_with_periods')->nullable();
            $table->string('year')->nullable();
            $table->string('place_of_delivery_or_abortion')->nullable();
            $table->string('hours_of_labor')->nullable();
            $table->string('type_of_delivery')->nullable();
            $table->string('complications_mother_infant')->nullable();
            $table->string('birth_weight')->nullable();
            $table->string('present_health')->nullable();
            $table->string('birth_control_currently_used')->nullable();
            $table->string('have_sexual_partner')->nullable();
            $table->string('concerns_about_sexual_activity')->nullable();
            $table->string('surgery')->nullable();
            $table->string('surgery_date')->nullable();
            $table->text('surgeries_and_date_or_none')->nullable();
            $table->json('other_past_gynecological_history')->nullable();
            $table->json('minor_discomfort')->nullable();
            $table->date('major_infection_when')->nullable();
            $table->string('major_infection_what')->nullable();
            $table->json('past_medical_history')->nullable();
            $table->string('use_smoke')->nullable();
            $table->string('packs_per_day')->nullable();
            $table->string('use_alcohol')->nullable();
            $table->string('glasses_per_day')->nullable();
            $table->string('use_illicit_drugs')->nullable();
            $table->string('type_of_drugs')->nullable();
            $table->string('amount')->nullable();
            $table->string('type_of_exercise')->nullable();
            $table->string('how_often')->nullable();
            $table->string('drug_allergy')->nullable();

            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_examinations');
    }
}
