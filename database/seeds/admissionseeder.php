<?php

use Illuminate\Database\Seeder;

class admissionseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admissions')->insert([
        'patient_id' => '6',
        'puft' => '38', 
        'parity' => '2', 
        'gravida' => '3', 
        'on_labor_with' => 'Blood and Mucoid', 
        'final_gravida' => '3', 
        'final_puft' => '0', 
        'final_parity' => '0', 
        'abortion' => '0', 
        'apgar_score' => '8', 
        'admitted_by' => 'Judith Vazquez', 
        'disposition' => 'Discharged', 
        'living' => '3', 
        'normal_vaginal_delivery' => 'Boy',
        'weight' => '3', 
        'created_at' => '2020-03-11 09:34:28', 
         ]);
         
     DB::table('admissions')->insert([
        'patient_id' => '8',
        'puft' => '40', 
        'parity' => '8', 
        'gravida' => '9', 
        'on_labor_with' => 'None', 
        'final_gravida' => '9', 
        'final_puft' => '40', 
        'final_parity' => '9', 
        'abortion' => '0', 
        'apgar_score' => '8', 
        'admitted_by' => 'Cayle Nixie Artocilla', 
        'disposition' => 'Discharged', 
        'living' => '9', 
        'normal_vaginal_delivery' => 'None',
        'weight' => '2.5', 
        'created_at' => '2020-03-12 10:05:46', 
         ]);
      
         DB::table('admissions')->insert([
          'patient_id' => '14',
         'puft' => '40', 
         'parity' => '8', 
         'gravida' => '9', 
         'on_labor_with' => 'None', 
         'final_gravida' => '9', 
         'final_puft' => '40', 
         'final_parity' => '9', 
         'abortion' => '0', 
         'apgar_score' => '8', 
         'admitted_by' => 'Cayle Nixie Artocilla', 
         'disposition' => 'Discharged', 
         'living' => '9', 
         'normal_vaginal_delivery' => 'None',
         'weight' => '2.5', 
         'created_at' => '2020-04-01 10:05:46', 
          ]);
  
  DB::table('admissions')->insert([
         'patient_id' => '15',
         'puft' => '40', 
         'parity' => '8', 
         'gravida' => '9', 
         'on_labor_with' => 'None', 
         'final_gravida' => '9', 
         'final_puft' => '40', 
         'final_parity' => '9', 
         'abortion' => '0', 
         'apgar_score' => '8', 
         'admitted_by' => 'Cayle Nixie Artocilla', 
         'disposition' => 'Discharged', 
         'living' => '9', 
         'normal_vaginal_delivery' => 'None',
         'weight' => '2.5', 
         'created_at' => '2020-04-02 10:05:46', 
          ]);
 
    }
}
