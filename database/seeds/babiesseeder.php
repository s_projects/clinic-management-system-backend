<?php

use Illuminate\Database\Seeder;

class babiesseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('babies')->insert([
            `baby_firstname` => 'Baby Boy',
            `baby_middlename` => 'Gementiza',
            `baby_lastname` => 'Tedlos', 
            `sex` => 'male',
            `time_of_delivery`=> '2:52 AM',
            `mother_name` => 'Tedlos, Elena Gementiza',
            `father_name` => 'Sitoy, Glenn',
            `address` => 'J.C. Homes, Cabaluna St., New Visayas, Panabo City',
            `circumference` => '0',
            `head_cm`=> '0',
            `chest_cm` => '0',
            `abdomin_cm` => '0',
            `length_cm` => '0',
            `apgar_score` => '0',
            `created_at` => '2020-03-11 09:39:19',
            `date_of_delivery`=> '2020-03-11',
              ]);
              
          DB::table('babies')->insert([
            `baby_firstname` => 'Baby Girl',
            `baby_middlename` => 'Lastima',
            `baby_lastname` => 'Banzon', 
            `sex` => 'female',
            `time_of_delivery`=> '10:49 PM',
            `mother_name` => 'Banzon, Elizabeth Lastima',
            `father_name` => 'Buagan, Bernardino',
            `address` => 'Maduao, Panabo City',
            `circumference` => '0',
            `head_cm`=> '29',
            `chest_cm` => '30',
            `abdomin_cm` => '27',
            `length_cm` => '46',
            `apgar_score` => '9',
            `created_at` => '2020-03-12 10:10:44',
            `date_of_delivery`=> '2020-03-12',
              ]);
              
              DB::table('babies')->insert([
                  `baby_firstname` => 'Jovit',
                  `baby_middlename` => 'Panlingan',
                  `baby_lastname` => 'Cotubate', 
                  `sex` => 'male',
                  `time_of_delivery`=> '06:30 AM',
                  `mother_name` => 'Cotubate, Dalia Panlingan',
                  `father_name` => 'Cotubate, John Dave',
                  `address` => 'Panabo City',
                  `circumference` => '0',
                  `head_cm`=> '30',
                  `chest_cm` => '31',
                  `abdomin_cm` => '27',
                  `length_cm` => '46',
                  `apgar_score` => '9',
                  `created_at` => '2020-04-01 10:10:44',
                  `date_of_delivery`=> '2020-04-01',
                    ]);
            
            DB::table('babies')->insert([
                  `baby_firstname` => 'Grace',
                  `baby_middlename` => 'Indino',
                  `baby_lastname` => 'Bustamante', 
                  `sex` => 'female',
                  `time_of_delivery`=> '10:00 PM',
                  `mother_name` => 'Bustamante, Aracita Indino',
                  `father_name` => 'Bustamante, Angelo',
                  `address` => 'Purok Golden Panabo City',
                  `circumference` => '0',
                  `head_cm`=> '29',
                  `chest_cm` => '29',
                  `abdomin_cm` => '27',
                  `length_cm` => '46',
                  `apgar_score` => '9',
                  `created_at` => '2020-04-02 10:10:44',
                  `date_of_delivery`=> '2020-04-02',
                    ]);
    }
}
