<?php

use App\OAuthClient;
use Illuminate\Database\Seeder;

class AuthClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
     * Run the database seeds.
     *
     * @return void
     */
        $oauth_client = OAuthClient::find(2);
        $oauth_client->secret = 'nIQK0Mlqn6Bw0vnbJ4bRwLlbDJzAzUvSMbghcNQA';
        $oauth_client->save();
    }
}
