<?php

use Illuminate\Database\Seeder;

class userseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Garcenila',
            'first_name' => 'May Ann',
            'middle_name' => 'E',
            'position' => 'admin',
            'email' => 'mgarcenila',
            'password' => bcrypt('garcenila123'),
	    'created_at' => '2020-03-09 06:03:22',
        ]);
	
	DB::table('users')->insert([
            'name' => 'Artocilla',
            'first_name' => 'Cayle Nixie',
            'middle_name' => 'L',
            'position' => 'staff',
            'email' => 'cartocilla',
            'password' => bcrypt('artocilla123'),
	    'created_at' => '2020-03-09 06:03:22',
        ]);

	DB::table('users')->insert([
            'name' => 'Vasquez',
            'first_name' => 'Judith',
            'middle_name' => 'C',
            'position' => 'admin',
            'email' => 'jvasquez',
            'password' => bcrypt('vasquez123'),
	    'created_at' => '2020-03-09 06:03:22',
        ]);

	DB::table('users')->insert([
            'name' => 'Lapating',
            'first_name' => 'Doreen Joy',
            'middle_name' => 'A',
            'position' => 'staff',
            'email' => 'dlapating',
            'password' => bcrypt('lapating123'),
	    'created_at' => '2020-03-09 06:03:22',
        ]);
    }
}
