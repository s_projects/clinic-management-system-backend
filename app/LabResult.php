<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class LabResult extends Model
{
    protected $guarded = [];

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id', 'id');
    }

    public function savePhoto(UploadedFile $file)
    {
        $filename = Str::random() . '.' . $file->getClientOriginalExtension();
        Storage::putFileAs('public/lab_results/' . $this->id, $file, $filename);
        $this->image = 'lab_results/' . $this->id . '/' . $filename;
        $this->save();
    }
}
