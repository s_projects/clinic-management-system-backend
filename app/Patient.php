<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $guarded = [];

    public function saveCaseNo()
    {
        $this->case_no = sprintf('%06d', $this->id);
        $this->save();
    }
}
