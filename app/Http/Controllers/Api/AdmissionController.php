<?php

namespace App\Http\Controllers\Api;

use App\Admission;
use App\Exports\AdmissionsExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admission\Store;
use App\Http\Requests\Admission\Update;
use PDF;

class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Admission::with(['patient'])->latest();

        if ($request->has('id')) {
            $items = $items->where('patient_id', $request->id);
        }

        if ($request->from) {
            $items = $items->whereDate('created_at', '>=', $request->from);
        }

        if ($request->to) {
            $items = $items->whereDate('created_at','<=', $request->to);
        }
        // if ($request->has('from') && $request->has('to')) {
        //     $items = $items->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
        // }

        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->paginate($limit);
        } else {
            $items= $items->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        try {
            $admission = Admission::create($request->all());
            return $admission;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        try {
            $admission = Admission::findOrFail($id);
            $admission->update($request->all());
            return response()->json($admission,200); 
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export(Request $request)
    {
        return (new AdmissionsExport($request->id, $request->date))->download("admissions_" . date('F_Y', strtotime($request->date)) . ".xlsx");
    }

    public function exportPdf(Request $request)
    {
        $year =  $request->year;
        $items = Admission::with(['patient'])
                            ->whereMonth('created_at', $request->month)
                            ->get();

        $pdf = PDF::loadView('admissions', [
            'admissions' => $items,
            'month' => date("F", mktime(0, 0, 0, $request->month, 1)),
            'year' => $year,
            'prepared' => $request->prepared,
            'generated' => $request->generated
            ])
            ->setPaper('a4', 'landscape');
            return $pdf->stream();
    }
}
