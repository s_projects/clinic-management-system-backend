<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LabResult\Store;
use App\Http\Requests\LabResult\Update;
use App\LabResult;
use App\Http\Resources\LabResultImage;

class LabResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = new LabResult();

        if($request->date) {
            $items = $items->whereDate('created_at', $request->date);
        }
        
        //search function 
        if (($request->has('s')) && ($request->has('s_field'))) {
            $fields= explode(',',$request['s_field']);
            foreach ($fields as $key => $field) {
                if ($key==0) {
                    $items= $items->where($field,'LIKE','%'.$request['s'].'%');
                } else {
                    $items= $items->orwhere($field,'LIKE','%'.$request['s'].'%');
                }
            }
        }

        if ($request->has('from') && $request->has('to')) {
            $items = $items->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
        }

        if ($request->has('id')) {
            $items = $items->where('patient_id', $request->id)->latest();
        }

        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->with('patient')->paginate($limit);
        } else {
            $items= $items->with('patient')->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return LabResultImage::collection($items);
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        try {
            $lab_result = LabResult::create($request->except('image'));
            if ($request->hasFile('image')) {
                $lab_result->savePhoto($request->file('image'));
            }
            return response()->json($lab_result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        try {
            if ($request->hasFile('image')) {
                $lab_result = LabResult::findOrFail($id);
                $lab_result->update($request->except('image'));
                $lab_result->savePhoto($request->file('image'));
            }
            return response()->json($lab_result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
