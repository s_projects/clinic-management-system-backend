<?php

namespace App\Http\Controllers\Api;

use App\BabyImmunization;
use App\Exports\BabyImmunizationsExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BabyImmunization\Store;
use App\Http\Requests\BabyImmunization\Update;
use PDF;

class BabyImmunizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = BabyImmunization::with(['baby'])->where('type_of_immunization', 'VitK, Eye Pro, Hepa B')
        ->orWhere('type_of_immunization', 'VitK, Eye Pro, Hepa B, BCG');

        if ($request->id) {
            $items = $items->where('baby_id', $request->id)->latest();
        }

        if ($request->from) {
            $items = $items->whereDate('created_at', '>=', $request->from);
        }

        if ($request->to) {
            $items = $items->whereDate('created_at','<=', $request->to);
        }

        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->paginate($limit);
        } else {
            $items= $items->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        try {
            $baby_immunization = BabyImmunization::create($request->all());

            // return $baby_immunization->baby->date_of_delivery;

            for ($i=0; $i < 6; $i++) { 
                switch ($i) {
                    case 0:
                        $schedule = date('Y-m-d', strtotime($baby_immunization->baby->date_of_delivery.'+2 days'));
                        BabyImmunization::create([
                            'baby_id' => $baby_immunization->baby_id,
                            'weight_grams' => 'N/A',
                            'type_of_immunization' => 'BCG',
                            'date_of_immunization' => $schedule,
                            'bcg_date' => $schedule,
                            'schedule_date' => $schedule
                        ]);
                        break;

                    case 1:
                        $schedule = date('Y-m-d', strtotime($baby_immunization->baby->date_of_delivery.'+1 month 15 days'));
                        BabyImmunization::create([
                            'baby_id' => $baby_immunization->baby_id,
                            'weight_grams' => 'N/A',
                            'type_of_immunization' => 'Pentavalent Vaccine, Oral Polio Vaccine, Pneumococcal Conjugate Vaccine',
                            'date_of_immunization' => $schedule,
                            'schedule_date' => $schedule
                        ]);
                        break;
                    
                    case 2:
                        $schedule = date('Y-m-d', strtotime($baby_immunization->baby->date_of_delivery.'+2 months 15 days'));
                        BabyImmunization::create([
                            'baby_id' => $baby_immunization->baby_id,
                            'weight_grams' => 'N/A',
                            'type_of_immunization' => 'Pentavalent Vaccine, Oral Polio Vaccine, Pneumococcal Conjugate Vaccine',
                            'date_of_immunization' => $schedule,
                            'schedule_date' => $schedule
                        ]);
                        break;
                    
                    case 3:
                        $schedule = date('Y-m-d', strtotime($baby_immunization->baby->date_of_delivery.'+3 months 15 days'));
                        BabyImmunization::create([
                            'baby_id' => $baby_immunization->baby_id,
                            'weight_grams' => 'N/A',
                            'type_of_immunization' => 'Pentavalent Vaccine, Oral Polio Vaccine, Inactivated Polio Vaccine Pneumococcal Conjugate Vaccine',
                            'date_of_immunization' => $schedule,
                            'schedule_date' => $schedule
                        ]);
                        break;
                    
                    case 4:
                        $schedule = date('Y-m-d', strtotime($baby_immunization->baby->date_of_delivery.'+9 months'));
                        BabyImmunization::create([
                            'baby_id' => $baby_immunization->baby_id,
                            'weight_grams' => 'N/A',
                            'type_of_immunization' => 'Measles, Mumps, Rubella',
                            'date_of_immunization' => $schedule,
                            'schedule_date' => $schedule
                        ]);
                        break;
                    
                    default:
                        $schedule = date('Y-m-d', strtotime($baby_immunization->baby->date_of_delivery.'+9 months 28 days'));
                        BabyImmunization::create([
                            'baby_id' => $baby_immunization->baby_id,
                            'weight_grams' => 'N/A',
                            'type_of_immunization' => 'Measles, Mumps, Rubella',
                            'date_of_immunization' => $schedule,
                            'schedule_date' => $schedule
                        ]);
                        break;
                }
            }
            return $baby_immunization;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        try {
            $baby_immunization = BabyImmunization::findOrFail($id);
            $baby_immunization->update($request->all());
            return response()->json($baby_immunization,200); 
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = BabyImmunization::findOrFail($id);
        $patient->delete();

        return response()->json(['message' => 'Patient record successfully deleted!']);
    }

    public function export(Request $request)
    {
        return (new BabyImmunizationsExport($request->id, $request->date))->download("babies_immunization_" . date('F_Y', strtotime($request->date)) . ".xlsx");
    }

    public function dateSchedule(Request $request)
    {
        try {
            $schedules = BabyImmunization::where('baby_id', $request->baby_id)
                                        ->where('schedule_date', '>=', date('Y-m-d'))
                                        ->with('baby')
                                        ->get();

            return response()->json($schedules, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    
    public function immunizationHistory(Request $request)
    {
        try {
            $schedules = BabyImmunization::where('baby_id', $request->baby_id)
                                        ->where('schedule_date', '<', date('Y-m-d'))
                                        ->with('baby')
                                        ->get();

            return response()->json($schedules, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function exportPdf(Request $request)
    {
        $year = date('Y');
        $items = BabyImmunization::whereMonth('created_at', $request->month)
                                ->whereYear('created_at', $year)
                                ->where('type_of_immunization', 'VitK, Eye Pro, Hepa B')
                                ->orWhere('type_of_immunization', 'BCG, VitK, Eye Pro, Hepa B')
                                ->orWhere('type_of_immunization', 'VitK, Eye Pro, Hepa B, BCG')
                                ->get();
      

        $pdf = PDF::loadView('babyImmunization', [
            'baby_immunizations' => $items,
            'month' => date("F", mktime(0, 0, 0, $request->month, 1)),
            'year' => $year,
            'prepared' => $request->prepared,
            'generated' => $request->generated
            ])
            ->setPaper('a4', 'landscape');
            return $pdf->stream();
    }
}
