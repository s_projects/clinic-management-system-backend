<?php

namespace App\Http\Controllers\Api;

use App\Baby;
use App\Patient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Prenatal;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function doLogin(Request $request)
    {
        $patient = Patient::where('username', $request->username)->first();
        // return $baby;
        if (!empty($patient)) {
            if (Hash::check($request->password, $patient->password)) {

                $baby = Baby::where('mother_name', $patient->lastname.', '.$patient->firstname.' '.$patient->middlename)->latest('date_of_delivery')->first();
                $prenatal = Prenatal::where('patient_id', $patient->id)->latest()->get();
                return response()->json([
                    "user_details" => $patient, 
                    "baby" => $baby,
                    "prenatal" => $prenatal,
                    'status' => 'patient'
                ], 200);
    
            } else {
                return response()->json(['message' => 'Invalid credentials!'], 500);
            }
        }  else {
            return response()->json(['message' => 'Credentials not found!'], 500);
        }
        
    }

    public function forgotPassword(Request $request)
    {
        try {
            $patient = Patient::where('username', $request->username)->first();
            if (!empty($patient)) {
                return response()->json(['user_details' => $patient, 'status' => true], 200);
            } else {
                return response()->json(['user_details' => $patient, 'status' => false], 200);
            }
            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    
    public function resetPassword(Request $request)
    {
        try {
            $patient = Patient::where('id', $request->user_id)->first();
            if (!empty($patient)) {
                $patient->update([
                    'password' => bcrypt($request->password)
                ]);
                return response()->json(['message' => 'Password updated successfully', 'status' => true], 200);
            } else {
                return response()->json(['message' => 'Something went wrong', 'status' => false], 200);
            }
            
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function changeUsername(Request $request)
    {
        try {
            $item = Patient::where('id', $request->user_id);
            if ($request->has('username')) {
                $item = $item->first();
                $item->update([
                    'username' => $request->username
                ]);

                return response()->json(['message' => 'Username updated successfully!', 'status' => true], 200);
            } else {
                return response()->json(['message' => 'Please fill all the fields!', 'status' => false], 200);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    
    public function changePassword(Request $request)
    {
        try {
            $patient = Patient::where('id', $request->user_id)->first();

            if (Hash::check($request->password, $patient->password)) {
                if ($request->new_password === $request->confirm_password) {
                    $patient->update([
                        'password' => bcrypt($request->new_password)
                    ]);
                    return response()->json(['message' => 'Password updated successfully!', 'status' => true], 200);
                } else {
                    return response()->json(['message' => 'New Password and Confirm Password not match!', 'status' => false], 200);
                }
                
            } else {
                return response()->json(['message' => 'Invalid Current Passsword!', 'status' => false], 200);
            }
            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
