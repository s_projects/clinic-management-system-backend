<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Store;
use App\Http\Requests\Patient\Update;
use App\Patient;
use PDF;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Patient::latest();
        $fromDate = $request->get('from');
        $toDate = $request->get('to');
        
        //search function 
        if (($request->has('s')) && ($request->has('s_field'))) {
            $fields= explode(',',$request['s_field']);
            foreach ($fields as $key => $field) {
                if ($key==0) {
                    $items= $items->where($field,'LIKE','%'.$request['s'].'%');
                } else {
                    $items= $items->orwhere($field,'LIKE','%'.$request['s'].'%');
                }
            }
        }

        // if ($request->has('from') && $request->has('to')) {
        //     $items = $item->whereBetween('created_at', [date('Y-m-d', $request->from), date('Y-m-d', $request->to)]);
        // }
        if ($request->has('from') && $request->has('to')) {
            $items = $items->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
        }

        if ($request->has('id')) {
            $items = $items->where('id', $request->id);
        }

        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->paginate($limit);
        } else {
            $items= $items->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        try {
            $request['username'] = $request->firstname.$request->lastname;
            $request['password'] = bcrypt($request->firstname.'123');
            $patient = Patient::create($request->all());

            if ($patient->id) {
                $patient->saveCaseNo();
            }
            
            return $patient;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        try {
            $patient = Patient::findOrFail($id);
            $patient->update($request->all());
            return response()->json($patient,200); 
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patient = Patient::findOrFail($id);
        $patient->delete();

        return response()->json(['message' => 'Patient record successfully deleted!']);
    }

    public function getCaseNo()
    {
        $case_no = Patient::select('case_no')->latest()->first();
        $case_no->case_no = sprintf('%06d', (int) $case_no->case_no + 1);

        return response()->json($case_no);
    }
    
    public function exportPdf(Request $request)
    {
        $items = Patient::where('id', $request->customer)->get();

        $pdf = PDF::loadView('patient', [
            'patients' => $items,
            'month' => date("F", mktime(0, 0, 0, $request->month, 1)),
            'prepared' => $request->prepared,
            'generated' => $request->generated
            ])
            ->setPaper('a4', 'landscape');
            return $pdf->stream();
    }
}
