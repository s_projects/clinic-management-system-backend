<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Prenatal;
use App\PrenatalVisit;

class PrenatalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = new Prenatal();
        
        //search function 
        if (($request->has('s')) && ($request->has('s_field'))) {
            $fields= explode(',',$request['s_field']);
            foreach ($fields as $key => $field) {
                if ($key==0) {
                    $items= $items->where($field,'LIKE','%'.$request['s'].'%');
                } else {
                    $items= $items->orwhere($field,'LIKE','%'.$request['s'].'%');
                }
            }
        }

        if ($request->has('from') && $request->has('to')) {
            $items = $items->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
        }

        if ($request->has('id')) {
            $items = $items->where('patient_id', $request->id)->with(['prenatalVisit','patient'])->latest()->get();
        }

        // check if the items has page and limit
        // if ($request->has('page')) {
        //     $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
        //     $items= $items->with('patient')->paginate($limit);
        // } else {
        //     $items= $items->with('patient')->get();
        // }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    public function getPrenatalAll(Request $request)
    {
        $items = new Prenatal();

        if ($request->has('id')) {
            $items = $items->where('patient_id', $request->id)->with(['patient']);
        }

        //check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->with('patient')->paginate($limit);
        } else {
            $items= $items->with('patient')->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // $request['date_schedule'] = date('Y-m-d', strtotime("+60 day", strtotime($request['lmp'])));
            $prenatal = Prenatal::create($request->all());
            return $prenatal;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $prenatal = Prenatal::findOrFail($id);
            $prenatal->update($request->all());
            return response()->json($prenatal,200); 
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $patient = Prenatal::findOrFail($id);
        // $patient->delete();

        // return response()->json(['message' => 'Patient record successfully deleted!']);
    }

    public function dateSchedule(Request $request)
    {
        try {
            $prenatal = Prenatal::where('patient_id', $request->patient_id)->latest()->first();
            if ($prenatal) {
                $schedules = PrenatalVisit::where('prenatal_id', $prenatal->id)
                            ->where('next_sched', '>=', date('Y-m-d'))
                            ->with('patient')
                            ->get();
                            
                return response()->json($schedules, 200);
            } else {
                // $schedules = PrenatalVisit::where('prenatal_id', $prenatal->id)
                //             ->where('next_sched', '>=', date('Y-m-d'))
                //             ->with('patient')
                //             ->get();
                            
                return response()->json([], 200);
            }
            
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function prenatalHistory(Request $request)
    {
        try {
            if ($request->type === 'mobile') {
                $prenatal = Prenatal::where('patient_id', $request->patient_id)->latest()->get();
                // dd($prenatal[1]->id);
                // $schedules = new PrenatalVisit();
                $collect_schedules = collect();
                $new_schedules = collect();
                $s = 0;
                for ($i=0; $i < count($prenatal); $i++) {
                    $schedules = PrenatalVisit::where('prenatal_id', $prenatal[$i]->id)
                                ->where('next_sched', '<', date('Y-m-d'))
                                ->latest()
                                ->get();
                    $collect_schedules->push($schedules);

                }
                // dd(count($collect_schedules));

                for ($i=0; $i < count($collect_schedules); $i++) { 
                    // return $collect_schedules[$i];
                    for ($j=0; $j < count($collect_schedules[$i]); $j++) { 
                        // return $collect_schedules[$i][$j]->first();
                        $new_schedules->push($collect_schedules[$i][$j]);
                    }
                }

                                
                return response()->json($new_schedules, 200);
            } else {
                $prenatal = Prenatal::where('patient_id', $request->patient_id)->latest()->first();
                $schedules = PrenatalVisit::where('prenatal_id', $prenatal->id)
                                ->where('next_sched', '<', date('Y-m-d'))
                                ->with('patient')
                                ->get();
                                
                return response()->json($schedules, 200);
            }
            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
