<?php

namespace App\Http\Controllers\Api;

use App\Baby;
use App\Exports\BabiesExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Baby\Store;
use App\Http\Requests\Baby\Update;
use PDF;

class BabyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Baby::latest();

        if ($request->has('id')) {
            $items = $items->where('id', $request->id);
        }

        if ($request->from) {
            $items = $items->whereDate('created_at', '>=', $request->from);
        }

        if ($request->to) {
            $items = $items->whereDate('created_at','<=', $request->to);
        }

        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->paginate($limit);
        } else {
            $items= $items->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        try {
            $request['username'] = $request->baby_firstname.$request->baby_middlename.$request->baby_lastname;
            $request['password'] = bcrypt($request->baby_firstname.'123');
            $baby = Baby::create($request->all());
            return $baby;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, $id)
    {
        try {
            $baby = Baby::findOrFail($id);
            $baby->update($request->all());
            return response()->json($baby,200); 
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $baby = Baby::findOrFail($id);
        $baby->delete();

        return response()->json(['message' => 'Baby record successfully deleted!']);
    }

    public function export(Request $request)
    {
        return (new BabiesExport($request->id, $request->date))->download("babies_" . date('F_Y', strtotime($request->date)) . ".xlsx");
    }

    public function exportPdf(Request $request)
    {
        $year = date('Y');
        $items = Baby::whereMonth('created_at', $request->month)->whereYear('created_at', $year)->get();

        $pdf = PDF::loadView('babies', [
            'babies' => $items,
            'month' => date("F", mktime(0, 0, 0, $request->month, 1)),
            'year' => $year,
            'prepared' => $request->prepared,
            'generated' => $request->generated
            ])
            ->setPaper('a4', 'landscape');
            return $pdf->stream();
    }
}
