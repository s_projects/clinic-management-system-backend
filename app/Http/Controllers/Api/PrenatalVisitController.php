<?php

namespace App\Http\Controllers\Api;

use App\PrenatalVisit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PrenatalVisitController extends Controller
{
    public function index(Request $request)
    {
        $items = new PrenatalVisit();
        
        //search function 
        if (($request->has('s')) && ($request->has('s_field'))) {
            $fields= explode(',',$request['s_field']);
            foreach ($fields as $key => $field) {
                if ($key==0) {
                    $items= $items->where($field,'LIKE','%'.$request['s'].'%');
                } else {
                    $items= $items->orwhere($field,'LIKE','%'.$request['s'].'%');
                }
            }
        }

        if ($request->has('from') && $request->has('to')) {
            $items = $items->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
        }

        if ($request->has('id')) {
            $items = $items->where('patient_id', $request->id)->latest('created_at');
        }

        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->with('patient')->paginate($limit);
        } else {
            $items= $items->with('patient')->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    public function store(Request $request)
    {
        try {
            $prenatal = PrenatalVisit::create($request->all());
            return $prenatal;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
