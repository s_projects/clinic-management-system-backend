<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\PregnancyHistory;
use Illuminate\Http\Request;

class PregnancyHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = new PregnancyHistory();
        
        //search function 
        if (($request->has('s')) && ($request->has('s_field'))) {
            $fields= explode(',',$request['s_field']);
            foreach ($fields as $key => $field) {
                if ($key==0) {
                    $items= $items->where($field,'LIKE','%'.$request['s'].'%');
                } else {
                    $items= $items->orwhere($field,'LIKE','%'.$request['s'].'%');
                }
            }
        }

        // if ($request->has('from') && $request->has('to')) {
        //     $items = $item->whereBetween('created_at', [date('Y-m-d', $request->from), date('Y-m-d', $request->to)]);
        // }
        if ($request->has('from') && $request->has('to')) {
            $items = $items->whereBetween('created_at', [date('Y-m-d', strtotime($request->from)), date('Y-m-d', strtotime($request->to))]);
        }

        if ($request->has('patient_id')) {
            $items = $items->where('patient_id', $request->patient_id)->latest();
        }

        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->paginate($limit);
        } else {
            $items= $items->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $pregnancy_history = PregnancyHistory::create($request->all());
            return $pregnancy_history;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $pregnancy_history = PregnancyHistory::findOrFail($id);
            $pregnancy_history->update($request->all());
            return response()->json($pregnancy_history,200); 
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pregnancy_history = PregnancyHistory::findOrFail($id);
        $pregnancy_history->delete();

        return response()->json(['message' => 'Record successfully deleted!']);
    }
}
