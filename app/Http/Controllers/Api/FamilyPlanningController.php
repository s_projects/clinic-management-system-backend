<?php

namespace App\Http\Controllers\Api;

use App\Exports\FamilyPlanningsExport;
use App\FamilyPlanning;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;

class FamilyPlanningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = FamilyPlanning::with(['patient']);
        
        if ($request->has('id')) {
            $items = $items->where('patient_id', $request->id)->with(['patient'])->latest();
        }

        if ($request->from) {
            $items = $items->whereDate('created_at', '>=', $request->from);
        }

        if ($request->to) {
            $items = $items->whereDate('created_at','<=', $request->to);
        }

        if($request->type) {
            $items = $items->where('method_use', $request->type);
        }

        // check if the items has page and limit
        if ($request->has('page')) {
            $limit= (!empty($request['limit'])) ? $request['limit'] : 15;
            $items= $items->paginate($limit);
        } else {
            $items= $items->get();
        }
         
        // check if the item is not empty
        if (!empty($items)) {
            try {
               return response()->json($items,200); 
           } catch(\Exception $e) {
               return response()->json("Error.",400);
           }
        } else {
            return response()->json("0 items found.",404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $family_planning = FamilyPlanning::create($request->all());
            return $family_planning;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $family_planning = FamilyPlanning::findOrFail($id);
            $family_planning->update($request->all());
            return response()->json($family_planning,200); 
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dateSchedule(Request $request)
    {
        try {
            $schedules = FamilyPlanning::where('patient_id', $request->patient_id)
                                        ->where('date_schedule', '>=', date('Y-m-d'))
                                        ->get();

            return response()->json($schedules, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function export(Request $request)
    {
        return (new FamilyPlanningsExport($request->id, $request->date))->download("family_planning_" . date('F_Y', strtotime($request->date)) . ".xlsx");
    }

    public function exportPdf(Request $request)
    {
        $year =  $request->year;
        $items = FamilyPlanning::with(['patient'])
                            ->orderBy('method_use')
                            ->whereMonth('created_at', $request->month) 
                            ->whereYear('created_at', $year)
                            ->get();

        if($request->type) {
            $items = $items->where('method_use', $request->type);
        }

        $pdf = PDF::loadView('familyPlanningPdf', [
            'family_plannings' => $items,
            'month' => date("F", mktime(0, 0, 0, $request->month, 1)),
            'year' => $year,
            'prepared' => $request->prepared,
            'generated' => $request->generated
            ])
            ->setPaper('a4', 'landscape');
            return $pdf->stream();
    }
}
