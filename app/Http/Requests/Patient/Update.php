<?php

namespace App\Http\Requests\Patient;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "firstname" => "required|string",
            "lastname" => "required|string",
            "middlename" => "required|string",
            "sex" => "required|string",
            "birthdate" => "required|date",
            "nationality" => "required|string",
            "occupation" => "required|string",
            "status" => "required|string",
            "place_of_birth" => "required|string",
            "age" => "required|string",
            "religion" => "required|string",
            "mobile_no" => "required|string",
            "patient_address" => "required|string",
        ];
    }
}
