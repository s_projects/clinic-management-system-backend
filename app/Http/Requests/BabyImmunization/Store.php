<?php

namespace App\Http\Requests\BabyImmunization;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'baby_id' => 'required|exists:babies,id',
            'weight_grams' => 'required|string',
            'type_of_immunization' => 'required|string',
            'date_of_immunization' => 'required|string'
        ];
    }
}
