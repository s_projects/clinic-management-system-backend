<?php

namespace App\Http\Requests\Baby;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'baby_firstname' => 'required|string',
            'baby_lastname' => 'required|string',
            'sex' => 'required|string',
            'time_of_delivery' => 'required',
            'address' => 'required|string',
            'date_of_delivery' => 'required|date',
        ];
    }
}
