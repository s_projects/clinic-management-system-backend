<?php

namespace App\Exports;

use App\Baby;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BabiesExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;
    protected $id;
    protected $date;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($id=null, $date)
    {
        $this->id = $id;
        $this->date = $date;
    }

    public function collection()
    {
        $items = new Baby();

        if ($this->id != null) {
            $items = $items->where('id', $this->id)->whereMonth('created_at', date('m', strtotime($this->date)));
        } else {
            $items = $items->whereMonth('created_at', date('m', strtotime($this->date)));
        }

        return $items->get();
    }

    public function map($items): array
    {
        return [
            'baby_name' => $items->baby_lastname.", ".$items->baby_firstname." ".$items->baby_middlename,
            'sex' => $items->sex,
            'time_of_delivery' => $items->time_of_delivery,
            'mother_name' => $items->mother_name,
            'father_name' => $items->father_name,
            'address' => $items->address,
            'circumference' => $items->circumference,
            'head_cm' => $items->head_cm,
            'chest_cm' => $items->chest_cm,
            'abdomin_cm' => $items->abdomin_cm,
            'length_cm' => $items->length_cm,
            'apgar_score' => $items->apgar_score,
            // 'weight' => $items->weight,
        ];
    }

    public function headings(): array
    {
        return [
            'Baby Name',
            'Sex',
            'Time of Delivery',
            'Mother Name',
            'Father Name',
            'Address',
            'Circumference',
            'Head (cm)',
            'Chest (cm)',
            'Abdomin (cm)',
            'Length (cm)',
            'APGAR Score',
        ];
    }
}
