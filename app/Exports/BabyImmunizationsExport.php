<?php

namespace App\Exports;

use App\BabyImmunization;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BabyImmunizationsExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;
    protected $id;
    protected $date;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($id=null, $date)
    {
        $this->id = $id;
        $this->date = $date;
    }
    
    public function collection()
    {
        $items = new BabyImmunization();

        if ($this->id != null) {
            $items = $items->where('baby_id', $this->id)->whereMonth('created_at', date('m', strtotime($this->date)));
        } else {
            $items = $items->whereMonth('created_at', date('m', strtotime($this->date)));
        }

        return $items->with('baby')->get();
    }

    public function map($items): array
    {
        return [
            'baby_name' => $items->baby->baby_lastname.", ".$items->baby->baby_firstname." ".$items->baby->baby_middlename,
            'weight_grams' => $items->weight_grams,
            'type_of_immunization' => $items->type_of_immunization,
            'date_of_immunization' => $items->date_of_immunization,
            'is_bcg' => $items->is_bcg,
            'bcg_date' => $items->bcg_date,
        ];
    }

    public function headings(): array
    {
        return [
            'Baby Name',
            'Wieght Grams',
            'Type of Immunization',
            'Date of Immunization',
            'Is BCG',
            'BCG Date',
        ];
    }
}
