<?php

namespace App\Exports;

use App\Admission;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AdmissionsExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;
    protected $id;
    protected $date;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($id=null, $date)
    {
        $this->id = $id;
        $this->date = $date;
    }

    public function collection()
    {
        $items = new Admission();

        if ($this->id != null) {
            $items = $items->where('patient_id', $this->id)->whereMonth('created_at', date('m', strtotime($this->date)));
        } else {
            $items = $items->whereMonth('created_at', date('m', strtotime($this->date)));
        }

        return $items->with('patient')->get();
    }

    public function map($items): array
    {
        return [
            'patient_name' => $items->patient->lastname.", ".$items->patient->firstname." ".$items->patient->middlename,
            'puft' => $items->puft,
            'parity' => $items->parity,
            'gravida' => $items->gravida,
            'on_labor_with' => $items->on_labor_with,
            'final_gravida' => $items->final_gravida,
            'final_puft' => $items->final_puft,
            'final_parity' => $items->final_parity,
            'abortion' => $items->abortion,
            'apgar_score' => $items->apgar_score,
            'admitted_by' => $items->admitted_by,
            'disposition' => $items->disposition,
            'living' => $items->living,
            'normal_vaginal_delivery' => $items->normal_vaginal_delivery,
            'weight' => $items->weight,
        ];
    }

    public function headings(): array
    {
        return [
            'Patient Name',
            'PUFT',
            'Parity',
            'gravida',
            'On Labor With',
            'Final Gravida',
            'Final PUFT',
            'Final Parity',
            'Abortion',
            'APGAR Score',
            'Admitted By',
            'Disposition',
            'Living',
            'Normal Vaginal Delivery',
            'Weight',
        ];
    }
}
