<?php

namespace App\Exports;

use App\FamilyPlanning;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class FamilyPlanningsExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;
    protected $id;
    protected $date;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($id=null, $date)
    {
        $this->id = $id;
        $this->date = $date;
    }
    
    public function collection()
    {
        $items = new FamilyPlanning();

        if ($this->id != null) {
            $items = $items->where('patient_id', $this->id)->whereMonth('created_at', date('m', strtotime($this->date)));
        } else {
            $items = $items->whereMonth('created_at', date('m', strtotime($this->date)));
        }

        return $items->with('patient')->get();
    }

    public function map($items): array
    {
        return [
            'patient_name' => $items->patient->lastname.", ".$items->patient->firstname." ".$items->patient->middlename,
            'number_of_children' => $items->number_of_children,
            'fp_acceptance' => $items->fp_acceptance,
            'method_use' => $items->method_use,
            'provider' => $items->provider,
        ];
    }

    public function headings(): array
    {
        return [
            'Patient Name',
            'Number of Children',
            'FP Acceptance',
            'Method Use',
            'Provider',
        ];
    }
}
