<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BabyImmunization extends Model
{
    protected $guarded = [];

    public function baby()
    {
        return $this->belongsTo(Baby::class, 'baby_id', 'id');
    }
}
