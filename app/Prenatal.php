<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prenatal extends Model
{
    protected $guarded = [];

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id', 'id');
    }

    public function prenatalVisit()
    {
        return $this->hasMany(PrenatalVisit::class, 'prenatal_id', 'id');
    }
}
