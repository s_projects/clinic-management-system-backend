<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<style type="text/css">
    .column {
        padding: 10px;
        float: left;
        width: 50%;
    }
    .row {
        width: 100%;
    }
    .footer {
    bottom: 0px;
    width: 100%;
    position: fixed;
    }
    body {
        font-family: "Times New Roman", Times, serif;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
    .total {
        background-color: #7ccdd7;
    }
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        font-size: 0.875em;
        text-align: center;
    }
    th {
        background-color: #009688;
        font-family: "Consolas";
        color: white;   
    }
    th, td {
        padding: 5px;
        text-align: left;
    }
    .heading {
        text-align: center;
        font-weight: bold;
    }
    .value {
        text-align: center;
    }
</style>
<body>
        <center>
            <!-- <div class="row"> -->
              <!-- <div class="column"> -->
                <!-- <img src="./image/jlr_logo.png" width="20%"/> -->
              <!-- </div> -->
              <!-- <div class="column"> -->
                <p><b style="font-size: 15x">JLR BIRTHING CLINIC</b> <br>
                <b style="font-size: 13px;font-weight: 100">Cabaluna St. New Visayas Panabo City</b><br><br>
                <b style="font-size: 13px;font-weight: 100;font-weight: bold">BABY IMMUNIZATION MONTHLY REPORT</b><br>
                <b style="font-size: 13px;font-weight: 100">District Panabo</b><br>
                <b style="font-size: 13px;font-weight: 100">Month of {{ $month. ' ' . $year}}</b>
                </p>
              <!-- </div>
            </div> -->
        </center>
    <br />
    <table style="width: 100%">
        <tr>
            <td class="heading" style="width: 10%">NAME OF PATIENTS</td>
            <td class="heading">ADDRESS</td>
            <td class="heading">DATE DELIVERY</td>
            <td class="heading">TIME DELIVERED</td>
            <td class="heading">WEIGHT GRAMS</td>
            <td class="heading">SEX</td>
            <td class="heading">EYE PRO</td>
            <td class="heading">HEPA E</td>
            <td class="heading">VITAMIN K</td>
            <td class="heading">BCG</td>
        </tr>
        @foreach($baby_immunizations as $baby_immunization)
        <tr>
            <td style="text-align: center">{{ $baby_immunization->baby->baby_firstname . ' ' . $baby_immunization->baby->baby_lastname }}</td>
            <td style="text-align: center">{{ $baby_immunization->baby->address }}</td>
            <td style="text-align: center">{{ $baby_immunization->created_at->toDateString() }}</td>
            <td style="text-align: center">{{ $baby_immunization->created_at->toTimeString() }}</td>
            <td style="text-align: center">{{ $baby_immunization->weight_grams }}</td>
            <td style="text-align: center">{{ $baby_immunization->baby->sex }}</td>
            <td style="text-align: center">{{ $baby_immunization->date_of_immunization }}</td>
            <td style="text-align: center">{{ $baby_immunization->date_of_immunization }}</td>
            <td style="text-align: center">{{ $baby_immunization->date_of_immunization }}</td>
            <td style="text-align: center">{{ $baby_immunization->bcg_date }}</td>
        </tr>
        @endforeach
    </table>
    <br /> <br />
    <div class="row">
      <div class="column">
        <b>Prepared By: {{ $prepared }}</b>
      </div>
    <footer class="footer">
    <small>Generated By: {{ $generated }}, {{ $month. ' ' . $year }}</small>
    </footer>
    </div>
</body>
</html>
